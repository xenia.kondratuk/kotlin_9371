//change the code to make the function start return the string "OK".
fun start(): String {
    return "OK"
}



//Make the function joinOptions() return the list in a JSON format (for example, [a, b, c]) by specifying only two arguments.
fun joinOptions(options: Collection<String>) =
        options.joinToString(prefix = "[", postfix = "]")



//Change the declaration of the foo function in a way that makes the code using foo compile
fun foo(name: String, number: Int = 42, toUpperCase: Boolean = false): String =
    (if (toUpperCase) name.uppercase() else name) + number
fun useFoo() = listOf(
        foo("a"),
        foo("b", number = 1),
        foo("c", toUpperCase = true),
        foo(name = "d", number = 2, toUpperCase = true)
)



//Replace the trimIndent call with the trimMargin call taking # as the prefix value so that the resulting string doesn't contain the prefix character
const val question = "life, the universe, and everything"
const val answer = 42

val tripleQuotedString = """
    #question = "$question"
    #answer = $answer""".trimMargin("#")

fun main() {
    println(tripleQuotedString)
}



// Using the month variable, rewrite this pattern in such a way that it matches the date in the format 13 JUN 1992 (two digits, one whitespace, a month abbreviation, one whitespace, four digits)
val month = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"

fun getPattern(): String = """\d{2}\ $month \d{4}"""



// rewrite the following Java code so that it only has one if expression
fun sendMessageToClient(
        client: Client?, message: String?, mailer: Mailer
) {
    val email = client?.personalInfo?.email
    if (email != null && message != null) {
        mailer.sendMessage(email, message)
    }
}

class Client(val personalInfo: PersonalInfo?)
class PersonalInfo(val email: String?)
interface Mailer {
    fun sendMessage(email: String, message: String)
}



// Specify Nothing return type for the failWithWrongAge function
fun failWithWrongAge(age: Int?): Nothing {
    throw IllegalArgumentException("Wrong age: $age")
}

fun checkAge(age: Int?) {
    if (age == null || age !in 0..150) failWithWrongAge(age)
    println("Congrats! Next year you'll be ${age + 1}.")
}



// Pass a lambda to the any function to check if the collection contains an even number
fun containsEven(collection: Collection<Int>): Boolean =
        collection.any { it % 2 == 0 }


