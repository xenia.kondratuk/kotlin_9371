// Add the function compareTo to the class MyDate to make it comparable
data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(other: MyDate) = when {
        year != other.year -> year - other.year
        month != other.month -> month - other.month
        else -> dayOfMonth - other.dayOfMonth
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}



// Using ranges implement a function that checks whether the date
//  is in the range between the first date and the last date (inclusive)
fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}



// Make the class DateRange implement Iterable<MyDate>, so that it can be iterated over. 
// Use the function MyDate.followingDate() defined in DateUtil.kt
class DateRange(val start: MyDate, val end: MyDate) : Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> = object : Iterator<MyDate> {
        var current: MyDate = start

        override fun hasNext(): Boolean = current <= end

        override fun next(): MyDate {
            val result = current
            current = current.followingDate()
            return result
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in DateRange(firstDate, secondDate)) {
        handler(date)
    }
}



//Implement date arithmetic and support adding years, weeks, and days to a date
//—




//Implement the function Invokable.invoke() to count the number of times it is invoked
class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
        numberOfInvocations++
        return this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
