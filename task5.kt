//Add a custom setter to PropertyExample.propertyWithCounter so that the counter property is incremented every time the propertyWithCounter is assigned
class PropertyExample() {
    var counter = 0
    var propertyWithCounter: Int? = null
        set(n) {
            field = n
            counter++
        }
}



//Add a custom getter to make the val lazy really lazy. It should be initialized by invoking initializer() during the first access.
class LazyProperty(val initializer: () -> Int) {
    var value: Int? = null
    val lazy: Int
        get() {
            if (value == null) {
                value = initializer()
            }
            return value!!
        }
}



//make the property lazy using delegates
class LazyProperty(val initializer: () -> Int) {
    val lazyValue: Int by lazy(initializer)
}




//Implement the methods of the class EffectiveDate so you can delegate to it
class D {
    var date: MyDate by EffectiveDate()
}

class EffectiveDate<R> : ReadWriteProperty<R, MyDate> {

    var timeInMillis: Long? = null

    override fun getValue(thisRef: R, property: KProperty<*>): MyDate {
        return timeInMillis!!.toDate()
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: MyDate) {
        timeInMillis = value.toMillis()
    }
}
