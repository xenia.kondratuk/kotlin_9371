//Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек. В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток
import kotlin.concurrent.thread
import java.util.concurrent.TimeUnit

fun main() {
    thread(start = true) {
        TimeUnit.SECONDS.sleep(1)
        println("world")
    }

    TimeUnit.SECONDS.sleep(2)
    println("hello ")
}



//Написать две функции с задержкой, которые будут возвращать 2 числа. В main написать блок кода, который будет суммировать вызов этих 2 функций, сначала написать последовательный вызов функций, вывести сумму и время работы, потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время



//Дополнить код, чтоб программа выводила след текст в консоль.
//«I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main: I'm tired of waiting! I'm running finally main: Now I can quit.»
import kotlinx.coroutines.*

fun main() = runBlocking {
    val job = launch { //корутина
        repeat(3) {
            delay(1000) //1 сек
            println("I'm sleeping $it ...")
        }
    }
    println("main: Waiting for the job to complete...")
    job.join()
    println("main: I'm tired of waiting!")
}


//выведется:
// main: Waiting for the job to complete...
// I'm sleeping 0 ...
// I'm sleeping 1 ...
// I'm sleeping 2 ...
// main: I'm tired of waiting!
